from django.urls import path
from news import views

urlpatterns = [
    path('', views.HomeNews.as_view(), name='index'),
    path('category/<int:category_id>/', views.NewsCategory.as_view(), name='category'),
    path('news/<int:pk>/', views.ViewNews.as_view(), name='view_news'),
]
