import requests
from bs4 import BeautifulSoup
from django.views.generic import ListView, DetailView
from .models import News, Category


URL = {
       'Разработка': 'https://habr.com/ru/flows/develop/',
       'Администрирование': 'https://habr.com/ru/flows/admin/',
       'Дизайн': 'https://habr.com/ru/flows/design/',
       'Менеджмент': 'https://habr.com/ru/flows/management/',
       'Маректинг': 'https://habr.com/ru/flows/marketing/',
       'Научпоп': 'https://habr.com/ru/flows/popsci/'
       }

HEADERS = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0',
           'accept': '*/*'}


def get_html(url, params=None):
    r = requests.get(url, headers=HEADERS, params=params)
    return r


def get_content(html, category_name):
    soup = BeautifulSoup(html, 'html.parser')
    items = soup.find_all('article', class_='post_preview')
    content = []
    exist_category_title = Category.objects.all().filter(title=category_name).exists()
    if not exist_category_title:
        Category.objects.create(title=category_name)
    category_title = Category.objects.get(title=category_name)
    for item in items:
        title = item.find('a', class_='post__title_link').get_text()
        photo = item.find('div', class_='post__body-cover')
        text = item.find('div', class_='post__text').get_text()
        if photo:
            photo = photo.find_next('img').get('src')
        else:
            photo = item.find('div', class_='post__text').find_next('img').get('src')
        content.append({'title': title, 'photo': photo, 'content': text, 'category': category_title})
        exist_title = News.objects.all().filter(title=title).exists()
        if not exist_title:
            News.objects.create(title=title, photo=photo, content=text, category=category_title)
    return content


def parse():
    news = []
    for key, value in URL.items():
        html = get_html(value)
        if html.status_code == 200:
            for page in range(1, 3):
                html = get_html(value + 'page' + str(page))
                news.extend(get_content(html.text, key))
        else:
            print('Error')
    return news


parse()


class HomeNews(ListView):
    model = News
    template_name = 'news/index.html'
    context_object_name = 'news'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_queryset(self):
        return News.objects.filter(is_published=True).select_related('category')


class NewsCategory(ListView):
    model = News
    template_name = 'news/index.html'
    context_object_name = 'news'
    allow_empty = False

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_queryset(self):
        return News.objects.filter(category_id=self.kwargs['category_id'], is_published=True).select_related('category')


class ViewNews(DetailView):
    model = News
    template_name = 'news/view_news.html'
    context_object_name = 'news_item'
