from django.core.mail import send_mail
from django.shortcuts import render, get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Category, Product, Basket, ProductInBasket, Order, Review
from .serializers import ProductSerializer, CategoriesSerializer, BasketSerializer, ProductInBasketSerializer, \
    OrderSerializer, ReviewSerializer
from rest_framework import mixins, generics, viewsets, status
from .premission import IsOwner


class CategoriesList(generics.GenericAPIView, mixins.ListModelMixin):
    queryset = Category.objects.all()
    serializer_class = CategoriesSerializer

    def get(self, request):
        return self.list(request)


class CategoryDetail(generics.GenericAPIView, mixins.RetrieveModelMixin):
    queryset = Category.objects.all()
    serializer_class = CategoriesSerializer
    lookup_field = 'pk'

    def get(self, request, pk):
        return self.retrieve(request)


class ProductsList(generics.GenericAPIView, mixins.ListModelMixin):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get(self, request):
        return self.list(request)


class ProductDetail(APIView):

    def get(self, request, pk):
        product = get_object_or_404(Product, pk=pk)
        product.count_rating
        product.save()
        serializer = ProductSerializer(product)
        return Response(serializer.data)

    def put(self, request, pk):
        permission_classes = [IsAuthenticated]
        basket = get_object_or_404(Basket, owner=request.user)
        product = get_object_or_404(Product, pk=pk)
        if ProductInBasket.objects.filter(product=product, basket=basket):
            product_in_basket = ProductInBasket.objects.get(product=product, basket=basket)
            product_in_basket.amount += 1
            product_in_basket.save()
        else:
            ProductInBasket.objects.create(product=product, basket=basket, amount=1)
        serializer = ProductSerializer(product)
        return Response(serializer.data)



class BasketListAndUpdate(generics.GenericAPIView):
    queryset = ProductInBasket.objects.all()
    serializer_class = ProductInBasketSerializer
    permission_classes = [IsAuthenticated]

    def get(self, request):
        basket = get_object_or_404(Basket, owner=request.user)
        products = ProductInBasket.objects.filter(basket=basket)
        serializer = ProductInBasketSerializer(products, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = ProductInBasketSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

    def delete(self, request):
        basket = get_object_or_404(Basket, owner=request.user)
        products = ProductInBasket.objects.filter(basket=basket)
        for product in products:
            product.delete()
        products = ProductInBasket.objects.filter(basket=basket)
        serializer = ProductInBasketSerializer(products, many=True)
        return Response(serializer.data)


class OrderCreate(generics.GenericAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [IsAuthenticated]

    def get(self, request):
        basket = get_object_or_404(Basket, owner=request.user)
        products = ProductInBasket.objects.filter(basket=basket)
        serializer = ProductInBasketSerializer(products, many=True)
        return Response(serializer.data)

    def post(self, request):
        basket = get_object_or_404(Basket, owner=request.user)
        products = ProductInBasket.objects.filter(basket=basket)
        my_order = Order.objects.create(owner=request.user)
        orders = 'Your order: '
        for product in products:
            my_order.products.add(product.product)
            orders += f'{product.product.name} '
            my_order.price += product.product.price * product.amount
            my_order.save()
            product.delete()
        orders += f'.\nYou should pay {my_order.price}'
        mail = send_mail('my_order', orders, 'test1960@internet.ru', [request.user.email],
                         fail_silently=False)
        serializer = OrderSerializer(my_order)
        return Response(serializer.data)


class ReviewCreate(generics.GenericAPIView, mixins.CreateModelMixin):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class ReviewUpdateDelete(generics.GenericAPIView, mixins.RetrieveModelMixin, mixins.UpdateModelMixin,
                         mixins.DestroyModelMixin):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = [IsAuthenticated, IsOwner]
    lookup_field = 'pk'

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
