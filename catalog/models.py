from decimal import Decimal
from django.contrib.auth.models import User
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class Product(models.Model):
    name = models.CharField(max_length=228)
    describe = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, default=Decimal("0.00"),
                                verbose_name='Цена')
    description = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to='image')
    category = models.ForeignKey('Category', on_delete=models.CASCADE, related_name='products')
    rating = models.DecimalField(default=Decimal('3.00'),
                                 max_digits=3, decimal_places=2
                                 )

    def __str__(self):
        return f'{self.name}'

    @property
    def count_rating(self):
        reviews = self.review.filter()
        marks = [review.mark for review in reviews]
        self.rating = sum(marks)//len(marks)
        return self.rating


class Category(MPTTModel):
    name = models.CharField(max_length=64, unique=True)
    parent = TreeForeignKey('self', verbose_name='parent', null=True, blank=True, related_name='children',
                            on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name}'

    class MPTTMeta:
        order_insertion_by = ['name']


class Basket(models.Model):
    owner = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.owner.username}"


class ProductInBasket(models.Model):
    basket = models.ForeignKey(Basket, on_delete=models.CASCADE, related_name='basket')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product_in_basket', null=True,
                                blank=True)
    amount = models.PositiveIntegerField(default=0)

    def __str__(self):
        return f'{self.basket.owner}: {self.product.name}, {self.amount}'


class Order(models.Model):
    owner = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    products = models.ManyToManyField(Product)
    price = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.owner}: {self.products}, {self.price}"


class Review(models.Model):
    author = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    feedback = models.CharField(max_length=228)
    image = models.ImageField(upload_to='image/%Y/%m/%d/')
    mark = models.PositiveIntegerField(default=0)
    date = models.DateTimeField(auto_now=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='review')

    def __str__(self):
        return f'{self.author}-{self.post.name}'
# Create your models here.
