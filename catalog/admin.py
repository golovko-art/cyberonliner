from django.contrib import admin
from .models import Product, Category, Basket, ProductInBasket, Order, Review

admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Basket)
admin.site.register(ProductInBasket)
admin.site.register(Order)
admin.site.register(Review)
# Register your models here.
