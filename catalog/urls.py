from django.urls import path, include
from .views import ProductsList, ProductDetail, CategoryDetail, CategoriesList, BasketListAndUpdate, OrderCreate, \
    ReviewUpdateDelete, ReviewCreate

urlpatterns = [
    path('categories/', CategoriesList.as_view()),
    path('category_detail/<int:pk>/', CategoryDetail.as_view()),
    path('products/', ProductsList.as_view()),
    path('product_detail/<int:pk>/', ProductDetail.as_view()),
    path('basket/', BasketListAndUpdate.as_view()),
    path('order/', OrderCreate.as_view()),
    path('create_review/', ReviewCreate.as_view()),
    path('review/<int:pk>', ReviewUpdateDelete.as_view())
]
