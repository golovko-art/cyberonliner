from rest_framework import serializers
from .models import Category, Product, Basket, ProductInBasket, Order, Review


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    category = serializers.SlugRelatedField(slug_field='name', read_only=True)
    review = ReviewSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = (
            'id',
            'name',
            'describe',
            'price',
            'description',
            'image',
            'category',
            'rating',
            'review'
        )

    def create(self, validated_data):
        return Product.objects.create(owner=self.context['request'].user, **validated_data)


class RecursiveSerializer(serializers.Serializer):

    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data


class FilterReviewListSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        data = data.filter(parent=None)
        return super().to_representation(data)


class CategoriesSerializer(serializers.ModelSerializer):
    children = RecursiveSerializer(many=True)
    products = ProductSerializer(many=True)

    class Meta:
        list_serializer_class = FilterReviewListSerializer
        model = Category
        fields = (
            'id',
            'name',
            'children',
            'products'
        )


class BasketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Basket
        fields = '__all__'

    def create(self, validated_data):
        return Basket.objects.create(owner=self.context['request'].user, **validated_data)


class ProductInBasketSerializer(serializers.ModelSerializer):
    product = serializers.SlugRelatedField(slug_field='name', read_only=True)

    class Meta:
        model = ProductInBasket
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    products = serializers.SlugRelatedField(slug_field='name', read_only=True, many=True)

    class Meta:
        model = Order
        fields = (
            'products',
            'price'
        )

    def create(self, validated_data):
        return Order.objects.create(owner=self.context['request'].user, **validated_data)
