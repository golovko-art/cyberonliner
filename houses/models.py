from django.db import models
from decimal import Decimal


class Houses(models.Model):
    author = models.ForeignKey('auth.user', blank=True, null=True, on_delete=models.CASCADE)
    address = models.CharField(max_length=256)
    price = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, default=Decimal("0.00"),
                                verbose_name='Цена')
    short_description = models.TextField(max_length=256)
    long_description = models.TextField()
    latitude = models.DecimalField(max_digits=9, decimal_places=2, blank=True, null=True, default=Decimal("0.00"))
    longitude = models.DecimalField(max_digits=9, decimal_places=2, blank=True, null=True, default=Decimal("0.00"))
    cover = models.ImageField(upload_to='house_images/', blank=True, null=True)

    def __str__(self):
        return self.address

    class Meta:
        verbose_name = 'Квартира/Дом'
        verbose_name_plural = 'Квартиры/Дома'
