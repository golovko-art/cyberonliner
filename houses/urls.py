from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from houses.views import add_house, all_houses, house_detail, delete_house, edit_house, all_filtered_houses

urlpatterns = [
    path('add/', add_house, name='add_house'),
    path('all/<int:page>', all_houses, name='all_houses'),
    path('detail/<int:house_pk>', house_detail, name='house_detail'),
    path('delete/<int:house_pk>', delete_house, name='delete_house'),
    path('edit/<int:house_pk>', edit_house, name='edit_house'),
    path('filtered_houses/<int:page>/<int:min_price>/<int:max_price>', all_filtered_houses, name='all_filtered_houses'),
]
