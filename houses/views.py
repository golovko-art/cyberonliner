from django.shortcuts import render, redirect
import requests
from houses.models import Houses
from houses.forms import HouseForm
from django.http import HttpResponse


def all_houses(request, page):
    if page == 0:
        page = 1
    houses = Houses.objects.all()
    filters = {'min_price': 0, 'max_price': 0}
    if request.method == 'POST':
        page = 1
        filter_houses = []
        filters['min_price'], filters['max_price'] = int(request.POST['min-price']), int(request.POST['max-price'])
        for house in houses:
            if int(request.POST['min-price']) <= house.price <= int(request.POST['max-price']):
                filter_houses.append(house)
        houses = filter_houses
    coordinates = '['
    for house in houses:
        coordinates += '{ position: { lat:' + str(house.latitude) + ', lng:' + str(house.longitude) + '}, map: map},'
    coordinates += ']'
    pages_count = [number+1 for number in range((len(houses)//8)+1)]
    return render(request, 'all_houses.html', {'houses_first': houses[8*(page-1):8*(page-1)+4],
                                               'houses_second': houses[8*(page-1)+4:8*page],
                                               'coordinates': coordinates, 'filters': filters, 'pages_count': pages_count,
                                               'current_page': {'page': page, 'next': page+1, 'previous': page-1}})


def add_house(request):
    if request.method == 'POST':
        form = HouseForm(request.POST, request.FILES)
        if form.is_valid():
            if 'cover' in request.FILES:
                form.cover = request.FILES['cover']
            address = request.POST['address']
            API_KEY = 'AIzaSyALRO5c6X6Jm6Rc26dooAl1T_f2RZysmU0'
            params = {
                'key': API_KEY,
                'address': address
            }
            base_url = 'https://maps.googleapis.com/maps/api/geocode/json?'
            response = requests.get(base_url, params=params)
            data = response.json()
            if data['status'] == 'OK':
                result = data['results'][0]
                location = result['geometry']['location']
                loc = {'lat': str(location['lat']), 'lng': str(location['lng'])}
                house = form.save(commit=False)
                house.author = request.user
                house.address = address
                house.latitude = loc['lat']
                house.longitude = loc['lng']
                house.save()
                return redirect('house_detail', house_pk=house.pk)
    else:
        if request.user.is_authenticated:
            form = HouseForm()
            return render(request, 'add_house.html', {'form': form})
        else:
            return HttpResponse('Не надо ломать нам сайт!')


def house_detail(request, house_pk):
    house = Houses.objects.get(pk=house_pk)
    if house.author.id == request.user.id:
        user_is_author = True
    else:
        user_is_author = False
    coordinates = '[{ position: { lat:' + str(house.latitude) + ', lng:' + str(house.longitude) + '}, map: map},]'
    return render(request, 'house_detail.html', {'house': house, 'coordinates': coordinates, 'user_is_author': user_is_author})


def delete_house(request, house_pk):
    house = Houses.objects.get(pk=house_pk)
    if request.user.id == house.author.id:
        house.delete()
        return redirect('all_houses')
    else:
        return HttpResponse('Не надо ломать нам сайт!')



def edit_house(request, house_pk):
    house = Houses.objects.get(pk=house_pk)
    if request.method == 'POST':
        form = HouseForm(request.POST, request.FILES, instance=house)
        if form.is_valid():
            address = request.POST['address']
            API_KEY = 'AIzaSyALRO5c6X6Jm6Rc26dooAl1T_f2RZysmU0'
            params = {
                'key': API_KEY,
                'address': address
            }
            base_url = 'https://maps.googleapis.com/maps/api/geocode/json?'
            response = requests.get(base_url, params=params)
            data = response.json()
            if data['status'] == 'OK':
                result = data['results'][0]
                location = result['geometry']['location']
                loc = {'lat': str(location['lat']), 'lng': str(location['lng'])}
                house = form.save(commit=False)
                house.author = request.user
                house.address = address
                house.latitude = loc['lat']
                house.longitude = loc['lng']
                house.save()
                return redirect('house_detail', house_pk=house.pk)
    else:
        form = HouseForm(instance=house)
        return render(request, 'edit_house.html', {'form': form})


def all_filtered_houses(request, page, min_price, max_price):
    if page == 0:
        page = 1
    houses = Houses.objects.all()
    filters = {'min_price': min_price, 'max_price': max_price}
    if request.method == 'POST':
        page = 1
        filters['min_price'], filters['max_price'] = int(request.POST['min-price']), int(request.POST['max-price'])
    filter_houses = []
    for house in houses:
        if int(filters['min_price']) <= house.price <= int(filters['max_price']):
            filter_houses.append(house)
    houses = filter_houses
    coordinates = '['
    for house in houses:
        coordinates += '{ position: { lat:' + str(house.latitude) + ', lng:' + str(house.longitude) + '}, map: map},'
    coordinates += ']'
    pages_count = [number+1 for number in range((len(houses)//8)+1)]
    return render(request, 'all_houses.html', {'houses_first': houses[8*(page-1):8*(page-1)+4],
                                               'houses_second': houses[8*(page-1)+4:8*page],
                                               'coordinates': coordinates, 'filters': filters, 'pages_count': pages_count,
                                               'current_page': {'page': page, 'next': page+1, 'previous': page-1}})
