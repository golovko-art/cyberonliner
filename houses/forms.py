from django import forms
from houses.models import Houses


class HouseForm(forms.ModelForm):
    class Meta:
        model = Houses
        fields = [
            'price',
            'short_description',
            'long_description',
            'cover'
        ]
