from django.shortcuts import render
import requests
import COVID19Py
from bs4 import BeautifulSoup


def index(request):
    url = ' https://www.nbrb.by/api/exrates/rates?periodicity=0'
    res = requests.get(url).json()
    currency = {
        'dollar': res[4],
        'euro': res[5],
        'rub': res[16],
        'uah': res[2]
    }

    covid19 = COVID19Py.COVID19()
    virus = {
        'belarus': covid19.getLocationByCountryCode("BY")[0],
        'russia': covid19.getLocationByCountryCode("RU")[0],
        'ukraine': covid19.getLocationByCountryCode("UA")[0]
    }

    city = 'Минск'
    if request.method == 'POST':
        city = request.POST['city']

    appid = '5e80ee000ff4b83bf6ad548ee899272a'
    url = 'https://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid=' + appid

    res = requests.get(url.format(city)).json()

    if res['cod'] != '404':
        weather_info = {
            'city': city,
            'temp': res['main']['temp'],
            'icon': res['weather'][0]['icon']
        }
    else:
        weather_info = {
            'city': 'Город не найден'
        }
    return render(request, 'index.html', {'currency': currency, 'virus': virus, 'info': weather_info})
